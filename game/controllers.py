import os
import shutil
from os import path
from django.conf import settings
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework.response import *
from rest_framework.exceptions import PermissionDenied
from .serializers import *
from .models import *
from login.OAuth import *
from datetime import datetime
from zipfile import ZipFile

curr_dir = os.getcwd()
class Games(viewsets.ModelViewSet):
    serializer_class = GameSerializer
    queryset = Game.objects.all()

    def create(self, request) :
        form_serializer = GameSerializer(data=request.data)
        if is_authenticated(request) :
            if form_serializer.is_valid():
                form_serializer.save()
            return Response(form_serializer.data, status=status.HTTP_201_CREATED)

        else:
            return Response({"Message": "Please login first"}, status=status.HTTP_401_UNAUTHORIZED)

    def list(self, request) :
        if is_authenticated(request) :
            qry = Game.objects.all()
            serializer = GameSerializer(qry, many=True)
            return Response({'result':serializer.data})

        else:
            return Response({"Message": "Please login first"}, status=status.HTTP_401_UNAUTHORIZED)

    def retrieve(self, request, pk) :
        if is_authenticated(request) :
            instance = Game.objects.get(id=pk)
            serializer = GameSerializer(instance)
            return Response(serializer.data)
        return Response({"Message": "Please login first"}, status=status.HTTP_401_UNAUTHORIZED)

        

    def update(self, request, *args, **kwargs):
        if is_authenticated(request):
            partial = kwargs.pop("partial", False)
            instance = self.get_object()
            instance.title = request.data.get("title")
            instance.description = request.data.get("description")
            instance.genre = request.data.get("genre")
            instance.price = request.data.get("price")
            instance.save()
            serializer = self.get_serializer(instance)
            if getattr(instance, '_prefetched_objects_cache', None):
                instance._prefetched_objects_cache = {}
                return Response(serializer.data)
            else:
                return Response(serializer.data)

        else:
            return Response({"Message": "Please login first"}, status=status.HTTP_401_UNAUTHORIZED)

    def destroy(self, request, pk) :
        if is_authenticated(request):

            try:
                instance = self.get_object()
                self.perform_destroy(instance)
            except:
                pass
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response({"Message": "Please login first"}, status=status.HTTP_401_UNAUTHORIZED)